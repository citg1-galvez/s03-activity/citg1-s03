package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServletActivity extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2737662280502206047L;

	public void init() throws ServletException{
		System.out.println("***************************");
		System.out.println("Initialized connection to database.");
		System.out.println("***************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws IOException{
		System.out.println("Hello from the calculator servlet.");
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String operation = req.getParameter("operation");
		int result = 0;
		boolean invalid = false;
		
		PrintWriter out = res.getWriter();
		
		
		switch(operation) {
			case "add": 
				result = num1 + num2;
				break;
			case "subtract": 
				result = num1 - num2;
				break;
			case "multiply": 
				result = num1 * num2;
				break;
			case "divide": 
				result = num1 / num2;
				break;
			default:
				invalid = true;
				break;
		}
		
		out.println("<p>The two numbers you provided are: " +num1+" , "+num2 + "</p>");
		out.println("<p>The operation that you wanted is: " +operation + "</p>");
		out.println("<p>The result is: " + (invalid?"Invalid":result) + "</p>");
		
		
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException {
		PrintWriter	out = res.getWriter();
		
		out.println("<h1>You are now using the calculator app</h1><br>"
				+ "	<p>To use the app, input two numbers and an operation.</p><br>"
				+ "	<p>Hit the submit button after filling in the details.</p><br>"
				+ "	<p>You will get the result shown in your browser.</p><br>");
	}
	
	
	public void destroy() {
		System.out.println("***************************");
		System.out.println("Disconnected from the database.");
		System.out.println("***************************");
	}
}
